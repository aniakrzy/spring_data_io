package edu.ib.springio.dto;

public class ProductDto {
    private String name;
    private float price;
    private boolean available;

    public String getName() {
        return name;
    }

    public float getPrice() {
        return price;
    }

    public boolean isAvailable() {
        return available;
    }
}
