package edu.ib.springio.service;


import edu.ib.springio.entity.Order;
import edu.ib.springio.repository.OrderRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class OrderManager {

    private OrderRepo orderRepo;

    @Autowired
    public OrderManager(OrderRepo orderRepo) {
        this.orderRepo = orderRepo;
    }

    public Optional<Order> findById(Long id) {
        return orderRepo.findById(id);
    }

    public Iterable<Order> findAll() {
        return orderRepo.findAll();
    }

    public Order save(Order order) {
        return orderRepo.save(order);
    }

    public void deleteById(Long id) {
        orderRepo.deleteById(id);
    }


}
