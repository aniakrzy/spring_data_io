package edu.ib.springio.controller;


import edu.ib.springio.ResourceNotFoundException;
import edu.ib.springio.dto.CustomerDto;
import edu.ib.springio.entity.Customer;
import edu.ib.springio.service.CustomerManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping
public class CustomerApi {

    private CustomerManager customerManager;

    @Autowired
    public CustomerApi(CustomerManager customerManager) {
        this.customerManager = customerManager;
    }

    @GetMapping("/api/customers/all")
    public Iterable<Customer> getAll() {
        return customerManager.findAll();
    }

    @GetMapping("/api/customers")
    public Optional<Customer> getById(@RequestParam Long id) {
        return customerManager.findById(id);
    }

    @PostMapping("/api/admin/customers")
    public Customer addCustomer(@RequestBody Customer customer) {
        return customerManager.save(customer);
    }
    @PutMapping("/api/admin/customers")
    public Customer updateCustomer(@RequestBody Customer customer) {
        return customerManager.save(customer);
    }

    @PatchMapping("/api/admin/customers")
    public void patchCustomer(@RequestParam Long id, @RequestBody CustomerDto customerDto) {
        Customer customer = customerManager.findById(id).orElseThrow(ResourceNotFoundException::new);
        boolean needUpdate = false;
        if (StringUtils.hasLength(customerDto.getName())){
            customer.setName(customerDto.getName());
            needUpdate = true;
        }

        if (StringUtils.hasLength(customerDto.getAddress())){
            customer.setAddress(customerDto.getAddress());
            needUpdate = true;
        }

        if (needUpdate){
            customerManager.save(customer);
        }

    }

    @DeleteMapping("/api/admin/customers")
    public void deleteCustomer(@RequestParam Long id) {
        customerManager.deleteById(id);
    }

}
